#!/usr/bin/env python3

import glob
import os
import sys

from jinja2 import Environment, FileSystemLoader

options = {
  'lang': list(),
  'sonar': {
    # Possible values are: info, minor, major, critical, blocker
    'minimum_severity': os.getenv('SONAR_MINIMUM_SEVERITY', default='minor')
  },
  'java': {
    'version': os.getenv('SAST_JAVA_VERSION', default='11'),
  },
  'eslint': {
    'channel': 'eslint-7'
  },
  'php': {
    'standards': os.getenv('PHP_STANDARDS', default='PSR1,PSR2,PHPCS'),
  },
  'python': {
    'version': os.getenv('SAST_PYTHON_VERSION', default='3'),
  }
}

env = Environment(loader=FileSystemLoader('/usr/local/etc/codeclimate/templates'))

def php():
  return list(glob.iglob('**/*.php', recursive=True))

def python():
  return list(glob.iglob('**/*.py', recursive=True))

def java():
  return list(glob.iglob('**/*.java', recursive=True))

def markdown():
  return list(glob.iglob('**/*.md', recursive=True))

def html():
  return (
    list(glob.iglob('**/*.html', recursive=True)) or
    list(glob.iglob('**/*.css', recursive=True)) or
    list(glob.iglob('**/*.scss', recursive=True)) or
    list(glob.iglob('**/*.sass', recursive=True)) or
    list(glob.iglob('**/*.sss', recursive=True))
  )

def scala():
  return list(glob.iglob('**/*.scala', recursive=True))

def swift():
  return list(glob.iglob('**/*.swift', recursive=True))

def c():
  return (
    list(glob.iglob('**/*.c', recursive=True)) or
    list(glob.iglob('**/*.cpp', recursive=True)) or
    list(glob.iglob('**/*.cxx', recursive=True)) or
    list(glob.iglob('**/*.h', recursive=True)) or
    list(glob.iglob('**/*.hpp', recursive=True)) or
    list(glob.iglob('**/*.hxx', recursive=True))
  )

if __name__ == '__main__':
  if list(glob.iglob('.codeclimate', recursive=False)):
    print("Use custom codeclimate configuration")
    sys.exit(0)

  print("Creating codeclimate configuration...")

  if php():
    options['lang'].append('php')
  if python():
    options['lang'].append('python')
  if java():
    options['lang'].append('java')
  if html():
    options['lang'].append('html')
  if markdown():
    options['lang'].append('markdown')
  if scala():
    options['lang'].append('scala')
  if swift():
    options['lang'].append('swift')
  if c():
    options['lang'].append('c')

  template = env.get_template('codeclimate.yml')
  output = template.render(options)

  with open('.codeclimate', 'w') as writer:
    writer.write(output)
