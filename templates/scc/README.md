# SCC

An Alpine based Docker image to run [scc](https://github.com/boyter/scc) in GitLab CI.

## Usage

```yaml
stages:
  - build
  - test

include:
  - remote: https://gitlab.com/code-report/ci-templates/raw/main/src/scc/Scc.gitlab-ci.yml
```

## License

This project is Open Source software released under the [Apache 2.0 license](LICENSE).
