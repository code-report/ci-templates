#!/bin/sh

if [ -z "${SCC_EXCLUDES}" ]; then
    SCC_EXCLUDES=".git,target,.mvn,mvnw,mvnw.cmd,node_modules,vendor"
fi

if [ -z "${SCC_OUTPUT_JSON}" ]; then
    SCC_OUTPUT_JSON="scc.json"
fi

scc --exclude-dir "${SCC_EXCLUDES}" --format-multi "tabular:stdout,json:${SCC_OUTPUT_JSON}"

{
    echo '# HELP scc_lines The number of lines'
    echo '# TYPE scc_lines counter'
    jq -r '.[] as $k | "scc_lines{lang=\"\($k.Name)\"} \($k.Lines)"' < scc.json

    echo ''
    echo '# HELP scc_code The number of code lines'
    echo '# TYPE scc_code counter'
    jq -r '.[] as $k | "scc_code{lang=\"\($k.Name)\"} \($k.Code)"' < scc.json

    echo ''
    echo '# HELP scc_comment The number of comment lines'
    echo '# TYPE scc_comment counter'
    jq -r '.[] as $k | "scc_comment{lang=\"\($k.Name)\"} \($k.Comment)"' < scc.json

    echo ''
    echo '# HELP scc_files The number of code files'
    echo '# TYPE scc_files counter'
    jq -r '.[] as $k | "scc_files{lang=\"\($k.Name)\"} \($k.Count)"' < scc.json

    echo ''
    echo '# HELP scc_complexity The value of code complexity'
    echo '# TYPE scc_complexity counter'
    jq -r '.[] as $k | "scc_complexity{lang=\"\($k.Name)\"} \($k.Complexity)"' < scc.json

    echo ''
    echo '# HELP scc_total The sum of each language'
    echo '# TYPE scc_total counter'
    jq -r '[.[].Count] | add as $v | "scc_total{key=\"files\"} \($v)"' < scc.json
    jq -r '[.[].Lines] | add as $v | "scc_total{key=\"lines\"} \($v)"' < scc.json
    jq -r '[.[].Code] | add as $v | "scc_total{key=\"code\"} \($v)"' < scc.json
    jq -r '[.[].Complexity] | add as $v | "scc_total{key=\"complexity\"} \($v)"' < scc.json
} > metrics.txt
